import os
import random

try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import tkMessageBox
except ImportError:
    from tkinter import messagebox as tkMessageBox


class MarketStudyVendor:

    def averagePrice(self, blog):
        if os.getenv('STUDY_LICENSE') is None:
            root = tk.Tk()
            root.overrideredirect(1)
            root.withdraw()
            tkMessageBox.showinfo(title="Missing license !", message="Missing license !")
            raise Exception("Missing licence")

        return random.random() * hashcode(blog)


def hashcode(s):
    h = 0
    for c in s:
        h = (31 * h + ord(c)) & 0xFFFFFFFF
    return ((h + 0x80000000) & 0xFFFFFFFF) - 0x80000000
