from .AdSpace import AdSpace
from .BlogAuctionTask import BlogAuctionTask


class AutomaticQuoteBot():
    def send_all_quotes(self, mode):
        blogs = AdSpace.get_AdSpaces()
        for blog in blogs:
            auctionTask = BlogAuctionTask()
            auctionTask.PriceAndPublish(blog, mode)
