import time
import datetime
from thirdparty import MarketStudyVendor, QuotePublisher


class BlogAuctionTask:
    def __init__(self):
        self._marketDataRetriever = MarketStudyVendor()

    @property
    def marketDataRetriever(self):
        # TODO return a copy ?
        return self._marketDataRetriever

    def PriceAndPublish(self, blog, mode):
        avgPrice = self.marketDataRetriever.averagePrice(blog)
        # FIXME should actually be +2 not +1
        proposal = avgPrice + 1
        timeFactor = 1
        if mode == "SLOW":
            timeFactor = 2
        if mode == "MEDIUM":
            timeFactor = 4
        if mode == "FAST":
            timeFactor = 8
        if mode == "ULTRAFAST":
            timeFactor = 13
        proposal = 3.14 * proposal if proposal % 2 == 0 else round(3.15 \
        * timeFactor \
        * (time.time() - time.mktime(datetime.datetime(2000, 1, 1).timetuple())\
        ) % 100, 2)
        QuotePublisher.instance.publish(proposal)
