from thirdparty.TechBlogs import TechBlogs


class AdSpace:
    cache = {}

    @staticmethod
    def get_AdSpaces():
        if "blogs list" in AdSpace.cache:
            return AdSpace.cache.get("blogs list")

        # FIXME : only return blogs that start with a 'T'
        list_all_blogs = TechBlogs.list_all_blogs()
        AdSpace.cache["blogs list"] = list_all_blogs
        return list_all_blogs
